use sea_orm_migration::prelude::*;

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .create_table(
                Table::create()
                    .table(CreateBooking::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(CreateBooking::Id)
                            .integer()
                            .not_null()
                            .unique_key()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(CreateBooking::RoomId).uuid().not_null())
                    .col(ColumnDef::new(CreateBooking::StartTime).timestamp().not_null())
                    .col(ColumnDef::new(CreateBooking::EndTime).timestamp().not_null())
                    .col(ColumnDef::new(CreateBooking::EmployeeName).string().not_null())
                    .col(ColumnDef::new(CreateBooking::Comment).string().null())
                    .to_owned(),
            )
            .await
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(CreateBooking::Table).to_owned())
            .await
    }
}

#[derive(DeriveIden)]
enum CreateBooking {
    Table,
    Id,
    RoomId,
    StartTime,
    EndTime,
    EmployeeName,
    Comment,
}
