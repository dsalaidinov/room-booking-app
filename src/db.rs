use dotenv::dotenv;
use sea_orm::{Database, DatabaseConnection};
use std::env;

pub async fn db_connection() -> DatabaseConnection {
    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("  must be set");
    Database::connect(database_url).await.unwrap()
}
