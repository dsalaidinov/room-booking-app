// handlers/login_handler.rs
use axum::{extract::Json,  response::{Json as JsonResponse, IntoResponse}};
use ldap3::{LdapConnAsync, LdapConnSettings, Scope, SearchEntry};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::error::Error;
use dotenv::dotenv;
use std::env;

#[derive(Deserialize)]
pub struct LoginRequest {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Serialize)]
pub struct UserInfo {
    pub username: String,
    pub full_name: Option<String>
}

pub async fn handle_login(Json(payload): Json<LoginRequest>) -> impl IntoResponse {
    match authenticate_ldap(&payload.username, &payload.password).await {
        Ok(user_info) => {
            // Successful authentication
            JsonResponse(json!({"message": "Login successful", "user_info": user_info})).into_response()
        }
        Err(err) => {
            // Failed authentication
            JsonResponse(json!({"error": err.to_string()})).into_response()
        }
    }
}

async fn authenticate_ldap(username: &str, password: &str) -> Result<UserInfo, Box<dyn Error>> {
    dotenv().ok();

    let ldap_host = env::var("LDAP_HOST")?;
    let base_dn = env::var("BASE_DN")?;
    let email = format!("{}@doscredobank.kg", username);

    let (conn, mut ldap_conn) = LdapConnAsync::with_settings(
        LdapConnSettings::new()
                .set_starttls(false)
                .set_no_tls_verify(true),
                &ldap_host).await?;

    ldap3::drive!(conn);

    let _ = ldap_conn.simple_bind(email.as_str(), password).await?.success();
    let ldap_filter = format!(r#"(sAMAccountName={})"#, username);

    let (rs, _res) = ldap_conn.search(
        &base_dn,
        Scope::Subtree,
        &ldap_filter.as_str(),
        vec!["sAMAccountName"]
    ).await?.success()?;

    let entry = rs.into_iter().next().ok_or("User not found")?;
    let user_info = SearchEntry::construct(entry);
    ldap_conn.unbind().await?;

    let full_name = user_info.dn.split(',')
        .find(|s| s.starts_with("CN="))
        .map(|s| s.trim_start_matches("CN="))
        .map(|s| s.to_string());

    let user = UserInfo {
        username: username.to_string().to_owned(),
        full_name
    };
    Ok(user)
}