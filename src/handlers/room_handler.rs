use axum::{extract::Path, http::header, http::StatusCode, response::IntoResponse, Json};
use sea_orm::{ActiveModelTrait, ColumnTrait, DatabaseConnection, EntityTrait, QueryFilter, Set};
use uuid::Uuid;

use crate::{
    db::db_connection,
    models::room_models::{CreateRoomModel, RoomModel, UpdateRoomModel},
};

use serde_json::json;

#[derive(Debug)]
pub struct APIError {
    pub message: String,
    pub status_code: StatusCode,
    pub error_code: Option<i8>,
}

impl IntoResponse for APIError {
    fn into_response(self) -> axum::response::Response {
        let status_code = self.status_code;
        (status_code,[(header::CONTENT_TYPE,"application/json")], Json(json!({ "StatusCode": self.status_code.as_u16(),"ErrorCode": self.error_code,"Message": self.message })) ).into_response()
    }
}

pub async fn create_room_post(Json(room_data): Json<CreateRoomModel>) -> impl IntoResponse {
    let db: DatabaseConnection = db_connection().await;

    let room_model = entity::room::ActiveModel {
        title: Set(room_data.title.to_owned()),
        description: Set(room_data.description.to_owned()),
        color: Set(room_data.color.to_owned()),
        uuid: Set(Uuid::new_v4()),
        ..Default::default()
    };

    room_model.insert(&db).await.unwrap();

    db.close().await.unwrap();
    (StatusCode::ACCEPTED, "Saved")
}

pub async fn update_room_put(
    Path(uuid): Path<Uuid>,
    Json(room_data): Json<UpdateRoomModel>,
) -> Result<(), APIError> {
    let db: DatabaseConnection = db_connection().await;

    let mut room: entity::room::ActiveModel = entity::room::Entity::find()
        .filter(entity::room::Column::Uuid.eq(uuid))
        .one(&db)
        .await
        .map_err(|err| APIError {
            message: err.to_string(),
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
            error_code: Some(50),
        })?
        .ok_or(APIError {
            message: "Not Found".to_owned(),
            status_code: StatusCode::NOT_FOUND,
            error_code: Some(44),
        })?
        .into();

    room.title = Set(room_data.title);
    room.description = Set(room_data.description);

    room.update(&db).await.map_err(|err| APIError {
        message: err.to_string(),
        status_code: StatusCode::INTERNAL_SERVER_ERROR,
        error_code: Some(50),
    })?;

    Ok(())
}

pub async fn delete_room_delete(Path(uuid): Path<Uuid>) -> Result<(), APIError> {
    let db: DatabaseConnection = db_connection().await;

    let room = entity::room::Entity::find()
        .filter(entity::room::Column::Uuid.eq(uuid))
        .one(&db)
        .await
        .map_err(|err| APIError {
            message: err.to_string(),
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
            error_code: Some(50),
        })?
        .ok_or(APIError {
            message: "Not Found".to_owned(),
            status_code: StatusCode::NOT_FOUND,
            error_code: Some(44),
        })?;

    entity::room::Entity::delete_by_id(room.id)
        .exec(&db)
        .await
        .map_err(|err| APIError {
            message: err.to_string(),
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
            error_code: Some(50),
        })?;

    Ok(())
}

pub async fn all_room_get() -> Result<Json<Vec<RoomModel>>, APIError> {
    let db: DatabaseConnection = db_connection().await;

    let rooms_result = entity::room::Entity::find().all(&db).await;

    match rooms_result {
        Ok(rooms) => {
            let room_models: Vec<RoomModel> = rooms
                .into_iter()
                .map(|item| RoomModel {
                    title: item.title,
                    description: item.description,
                    color: item.color,
                    uuid: item.uuid,
                })
                .collect();

            Ok(Json(room_models))
        }
        Err(err) => {
            eprintln!("Error fetching rooms: {}", err);
            Err(APIError {
                message: err.to_string(),
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                error_code: Some(50),
            })
        }
    }
}
