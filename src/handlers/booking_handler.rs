use axum::{extract::Path, http::{header, StatusCode}, response::IntoResponse, Json};
use chrono::NaiveDateTime;
use serde::Deserialize;
use crate::{
    db::db_connection,
    models::{
        booking_models::{BookingModel, CreateBookingModel, UpdateBookingModel},
        room_models::RoomModel,
    },
};
use sea_orm::{ActiveModelTrait, Condition, DatabaseConnection, EntityTrait, QueryFilter,ColumnTrait, Set};
use serde_json::json;

#[derive(Debug)]
pub struct APIError {
    pub message: String,
    pub status_code: StatusCode,
    pub error_code: Option<i8>,
}

#[derive(Deserialize)]
pub struct PathDate {
    date: NaiveDateTime,
}

impl IntoResponse for APIError {
    fn into_response(self) -> axum::response::Response {
        let status_code = self.status_code;
        (
            status_code,
            [(header::CONTENT_TYPE, "application/json")],
            Json(json!({
                "StatusCode": self.status_code.as_u16(),
                "ErrorCode": self.error_code,
                "Message": self.message,
            })),
        )
            .into_response()
    }
}

pub async fn create_booking_post(
    Json(booking_data): Json<CreateBookingModel>,
) -> Result<Json<CreateBookingModel>, APIError> {
    let db: DatabaseConnection = db_connection().await;
    let booking_model = entity::create_booking::ActiveModel {
        room_id: Set(booking_data.room_id),
        start_time: Set(booking_data.start_time),
        end_time: Set(booking_data.end_time),
        employee_name: Set(booking_data.employee_name),
        comment: Set(booking_data.comment),
        ..Default::default()
    };

    let created_booking = booking_model.insert(&db).await.map_err(|err| APIError {
        message: err.to_string(),
        status_code: StatusCode::INTERNAL_SERVER_ERROR,
        error_code: Some(50),
    })?;

    // Преобразование созданной брони в CreateBookingModel
    let created_booking_model = CreateBookingModel {
        room_id: created_booking.room_id,
        start_time: created_booking.start_time,
        end_time: created_booking.end_time,
        employee_name: created_booking.employee_name,
        comment: created_booking.comment,
    };

    // Возвращение созданной брони
    Ok(Json(created_booking_model))
}

pub async fn update_booking_put(
    Path(id): Path<i32>,
    Json(booking_data): Json<UpdateBookingModel>,
) -> Result<Json<serde_json::Value>, APIError> {
    let db: DatabaseConnection = db_connection().await;

    let mut booking: entity::create_booking::ActiveModel =
        entity::create_booking::Entity::find_by_id(id)
            .one(&db)
            .await
            .map_err(|err| APIError {
                message: err.to_string(),
                status_code: StatusCode::INTERNAL_SERVER_ERROR,
                error_code: Some(50),
            })?
            .ok_or(APIError {
                message: "Booking not found".to_owned(),
                status_code: StatusCode::NOT_FOUND,
                error_code: Some(44),
            })?
            .into();

    if let Some(start_time) = booking_data.start_time {
        booking.start_time = Set(start_time);
    }

    if let Some(end_time) = booking_data.end_time {
        booking.end_time = Set(end_time);
    }

    if let Some(comment) = booking_data.comment {
        booking.comment = Set(Some(comment));
    }

    booking.update(&db).await.map_err(|err| APIError {
        message: err.to_string(),
        status_code: StatusCode::INTERNAL_SERVER_ERROR,
        error_code: Some(50),
    })?;

    let message = json!({ "message": "Бронирование обновлено" });

    Ok(Json(message))
}

pub async fn delete_booking_delete(
    Path(id): Path<i32>,
) -> Result<Json<serde_json::Value>, APIError> {
    let db: DatabaseConnection = db_connection().await;

    let booking = entity::create_booking::Entity::find_by_id(id)
        .one(&db)
        .await
        .map_err(|err| APIError {
            message: err.to_string(),
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
            error_code: Some(50),
        })?
        .ok_or(APIError {
            message: "Booking not found".to_owned(),
            status_code: StatusCode::NOT_FOUND,
            error_code: Some(44),
        })?;

    entity::create_booking::Entity::delete_by_id(booking.id)
        .exec(&db)
        .await
        .map_err(|err| APIError {
            message: err.to_string(),
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
            error_code: Some(50),
        })?;

    let message = json!({ "message": "Бронирование удалено" });

    Ok(Json(message))
}

pub async fn all_bookings_get(Path(date): Path<PathDate>) -> Result<Json<Vec<BookingModel>>, APIError> {
    let db: DatabaseConnection = db_connection().await;

    let condition = Condition::all()
    .add(entity::create_booking::Column::StartTime.gt(date.date));

    // Get all bookings
    let bookings = entity::create_booking::Entity::find()
        .filter(condition)
        .all(&db)
        .await
        .map_err(|err| APIError {
            message: err.to_string(),
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
            error_code: Some(50),
        })?;

    // Convert bookings into models
    let booking_models: Vec<CreateBookingModel> = bookings
        .into_iter()
        .map(|booking| CreateBookingModel {
            room_id: booking.room_id,
            start_time: booking.start_time,
            end_time: booking.end_time,
            employee_name: booking.employee_name,
            comment: booking.comment,
        })
        .collect();

    // Get all the rooms
    let rooms = entity::room::Entity::find()
        .all(&db)
        .await
        .map_err(|err| APIError {
            message: err.to_string(),
            status_code: StatusCode::INTERNAL_SERVER_ERROR,
            error_code: Some(50),
        })?;

    let bookings_with_rooms: Vec<BookingModel> = booking_models
        .into_iter()
        .filter_map(|booking| {
            // Find the appropriate room to book
            if let Some(room) = rooms.iter().find(|&room| room.uuid == booking.room_id) {
                Some(BookingModel {
                    start_time: booking.start_time,
                    end_time: booking.end_time,
                    employee_name: booking.employee_name,
                    comment: booking.comment,
                    room_id: booking.room_id,
                    room: RoomModel {
                        title: room.title.to_owned(),
                        description: room.description.to_owned(),
                        color: room.color.to_owned(),
                        uuid: room.uuid,
                    },
                })
            } else {
                None
            }
        })
        .collect();

    Ok(Json(bookings_with_rooms))
}