use axum::Router;

mod db;
mod handlers;
mod models;
mod routes;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    server().await;
}

async fn server() {
    let app: Router = Router::new()
        .merge(routes::room_routes::room_routes())
        .merge(routes::booking_room_routes::booking_room_routes())
        .merge(routes::login_routes::login_routes());

    let listener = tokio::net::TcpListener::bind("localhost:4000").await.unwrap();

    println!("Server started");

    axum::serve(listener, app).await.unwrap();
}
