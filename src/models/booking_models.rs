use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use super::room_models::RoomModel;

#[derive(Serialize, Deserialize)]
pub struct CreateBookingModel {
    pub room_id: Uuid,
    pub start_time: NaiveDateTime,
    pub end_time: NaiveDateTime,
    pub employee_name: String,
    pub comment: Option<String>,
}

#[derive(Serialize, Deserialize)]
pub struct UpdateBookingModel {
    pub start_time: Option<chrono::NaiveDateTime>,
    pub end_time: Option<chrono::NaiveDateTime>,
    pub comment: Option<String>,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct BookingModel {
    pub room_id: Uuid,
    pub room: RoomModel,
    pub start_time: NaiveDateTime,
    pub end_time: NaiveDateTime,
    pub employee_name: String,
    pub comment: Option<String>,
}
