use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct RoomModel {
    pub title: String,
    pub description: String,
    pub color: String,
    pub uuid: Uuid
}

#[derive(Serialize, Deserialize)]
pub struct CreateRoomModel {
    pub title: String,
    pub description: String,
    pub color: String,
}

#[derive(Serialize, Deserialize)]
pub struct UpdateRoomModel {
    pub title: String,
    pub description: String,
    pub color: String,
}