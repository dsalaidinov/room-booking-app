use axum::{http::{header, Method}, routing::{delete, get, post, put}, Router};
use tower_http::cors::{CorsLayer, Any};
use crate::handlers::booking_handler;

pub fn booking_room_routes() -> Router {
    let cors = CorsLayer::new()
    .allow_headers(vec![
        header::ACCEPT,
        header::ACCEPT_LANGUAGE,
        header::AUTHORIZATION,
        header::CONTENT_LANGUAGE,
        header::CONTENT_TYPE,
    ])
    .allow_methods(vec![
        Method::GET,
        Method::POST,
        Method::PUT,
        Method::DELETE,
        Method::HEAD,
        Method::OPTIONS,
        Method::CONNECT,
        Method::PATCH,
        Method::TRACE,
    ])
    .allow_origin(Any);

    let booking_router = Router::new()
        .route("/api/booking/create", post(booking_handler::create_booking_post))
        .route("/api/booking/:uuid/update", put(booking_handler::update_booking_put))
        .route("/api/booking/:uuid/delete", delete(booking_handler::delete_booking_delete))
        .route("/api/booking/all/:date",get(booking_handler::all_bookings_get))
        .layer(cors);

    booking_router
}
