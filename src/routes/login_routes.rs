// routes/login_routes.rs
use axum::{http::{header, Method}, routing::post, Router};
use tower_http::cors::{Any, CorsLayer};
use crate::handlers::login_handler;

pub fn login_routes() -> Router {
    let cors = CorsLayer::new()
    .allow_headers(vec![
        header::ACCEPT,
        header::ACCEPT_LANGUAGE,
        header::AUTHORIZATION,
        header::CONTENT_LANGUAGE,
        header::CONTENT_TYPE,
    ])
    .allow_methods(vec![
        Method::GET,
        Method::POST,
        Method::PUT,
        Method::DELETE,
        Method::HEAD,
        Method::OPTIONS,
        Method::CONNECT,
        Method::PATCH,
        Method::TRACE,
    ])
    .allow_origin(Any);

    let login_router = Router::new()
        .route("/api/user/login", post(login_handler::handle_login))
        .layer(cors);
    
    login_router
}
