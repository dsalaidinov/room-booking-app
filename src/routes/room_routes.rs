use axum::http::header;
use axum::routing::{delete, put, get, post};
use axum::{Router, http::Method};
use tower_http::cors::{CorsLayer, Any};
use crate::handlers::room_handler;

pub fn room_routes() -> Router {

    let cors = CorsLayer::new()
    .allow_headers(vec![
        header::ACCEPT,
        header::ACCEPT_LANGUAGE,
        header::AUTHORIZATION,
        header::CONTENT_LANGUAGE,
        header::CONTENT_TYPE,
    ])
    .allow_methods(vec![
        Method::GET,
        Method::POST,
        Method::PUT,
        Method::DELETE,
        Method::HEAD,
        Method::OPTIONS,
        Method::CONNECT,
        Method::PATCH,
        Method::TRACE,
    ])
    .allow_origin(Any);

    let router = Router::new()
    .route("/api/room/create",post(room_handler::create_room_post))
    .route("/api/room/:uuid/update",put(room_handler::update_room_put))
    .route("/api/room/:uuid/delete",delete(room_handler::delete_room_delete))
    .route("/api/room/all",get(room_handler::all_room_get))
    .layer(cors);
    router
}